
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    InputControls _controls;
    bool controlBlock = false;
    [SerializeField] List<int> parameters = new List<int>();
    [SerializeField] float parameterChangeSpeed = 2f;
    bool isDead = false;
    int days = 1;
    [SerializeField] Color colorIncrease;
    [SerializeField] Color colorDecrease;
    Color colorWhite = Color.white;

    [SerializeField] GameObject gameEventPrefab;
    [ShowInInspector] GameEvent curEvent;
    [ShowInInspector] GameEvent nextEvent;

    [FoldoutGroup("UI Elements")]
    [SerializeField]
    List<Image> parametersUI = new List<Image>();
    [FoldoutGroup("UI Elements")]
    [SerializeField]
    TextMeshProUGUI daysCounter;

    private void Awake()
    {
        _controls = new InputControls();

        _controls.Action.Choise.performed += ctx => ChoiseAnimate(ctx.ReadValue<Vector2>());
        _controls.Action.Choise.canceled += ctx => CancelAnimate(ctx.ReadValue<Vector2>());

        _controls.Action.ConfirmLeftWASD.performed += ctx => ChooseConfirm(new Vector2(-1, 0));
        _controls.Action.ConfirmLeftArrow.performed += ctx => ChooseConfirm(new Vector2(-1, 0));
        _controls.Action.ConfirmRightWASD.performed += ctx => ChooseConfirm(new Vector2(1, 0));
        _controls.Action.ConfirmRightArrow.performed += ctx => ChooseConfirm(new Vector2(1, 0));
        _controls.Action.ConfirmUpWASD.performed += ctx => ChooseConfirm(new Vector2(0, 1));
        _controls.Action.ConfirmUpArrow.performed += ctx => ChooseConfirm(new Vector2(0, 1));
        _controls.Action.ConfirmDownWASD.performed += ctx => ChooseConfirm(new Vector2(0, -1));
        _controls.Action.ConfirmDownArrow.performed += ctx => ChooseConfirm(new Vector2(0, -1));

    }

    void Start()
    {
        parameters.Add(50);
        parameters.Add(50);
        parameters.Add(50);
        parameters.Add(50);
        curEvent = Instantiate(gameEventPrefab, Vector3.zero, Quaternion.identity).GetComponent<GameEvent>();
        curEvent.Randomize(1);
        days = 1;
        daysCounter.text = "Day " + days;
        isDead = false;
    }

    private void Update()
    {
        for (int i = 0; i < parameters.Count; i++)
        {
            parametersUI[i].fillAmount = Mathf.Lerp(parametersUI[i].fillAmount, (float)parameters[i] / 100f, parameterChangeSpeed * Time.deltaTime);
        }
    }
    public void ChooseConfirm(Vector2 choise)
    {
        if (curEvent.type != GameEventType.Death) {
            if (nextEvent == null)
            {
                nextEvent = Instantiate(gameEventPrefab, Vector3.zero, Quaternion.identity).GetComponent<GameEvent>();
                nextEvent.Randomize(-1);
            }
            if (choise.y == -1 && curEvent.hasDownAction)
            {
                (curEvent, nextEvent) = (nextEvent, curEvent);
                curEvent.StartCoroutine(curEvent.MoveLayer(1));
                nextEvent.StartCoroutine(nextEvent.MoveLayer(-1));
            }
            else
            {
                Dictionary<ParameterType, int> result = curEvent.Confirm(choise.x);
                if (result != null)
                {
                    curEvent.StartCoroutine(curEvent.MoveConfirm((int)choise.normalized.x, nextEvent));
                    foreach (int i in result.Keys)
                    {
                        parameters[i] += result[(ParameterType)i];
                        foreach (int p in parameters)
                        {
                            if (p <= 0 || p >= 100)
                            {
                                isDead = true;
                                Destroy(nextEvent.gameObject);
                                nextEvent = Instantiate(gameEventPrefab, Vector3.zero, Quaternion.identity).GetComponent<GameEvent>();
                                nextEvent.DeathEvent(parameters.IndexOf(p), p);
                                break;
                            }
                        }
                    }
                    curEvent = nextEvent;
                    nextEvent = null;
                    days++;
                    daysCounter.text = "Day " + days;
                    //daysCounter.text = "Day " + choise.x + choise.y;
                }
            } 
        }
        else
        {
            // do restart or menu
            if (choise.x < 0)
            {
                curEvent.StartCoroutine(curEvent.MoveConfirm(-1, nextEvent));
                LevelManager.GetInstance().LoadMenu();
            }
            else if (choise.x > 0)
            {
                curEvent.StartCoroutine(curEvent.MoveConfirm(1, nextEvent));
                LevelManager.GetInstance().ReloadGame();
            }
        }
    }

    void ChoiseAnimate(Vector2 choise)
    {
        if (!controlBlock)
        {
            controlBlock = true;
            curEvent.ChoiseAnimate(choise, true);
            if (choise.x != 0) {
                Dictionary<ParameterType, int> changeParameter = (choise.x < 0 ? curEvent.LeftEffect : curEvent.RightEffect);
                foreach(int i in changeParameter.Keys)
                {
                    parametersUI[i].color = (changeParameter[(ParameterType)i] == 0 ? colorWhite : (changeParameter[(ParameterType)i] > 0 ? colorIncrease : colorDecrease));
                }
            }
        }
    }

    void CancelAnimate(Vector2 choise)
    { 
        controlBlock = false;
        curEvent.ChoiseAnimate(choise, false);
        foreach (Image image in parametersUI)
        {
            image.color = colorWhite;
        }
    }

    private void OnEnable()
    {
        _controls.Action.Enable();
    }

    private void OnDisable()
    {
        _controls.Action.Disable();
    }
}

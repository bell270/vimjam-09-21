using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "New Game Event", menuName = "Game Event")]
public class GameEventTemplate : SerializedScriptableObject
{
    [HorizontalGroup("Basic Info")]
    [VerticalGroup("Basic Info/Right"), LabelWidth(150)]
    [SerializeField]
    public GameEventType type;
    [VerticalGroup("Basic Info/Left")]
    [PreviewField(100)]
    [HideLabel]
    [SerializeField]
    public Sprite Art;


    [FoldoutGroup("Text Vars")]
    [SerializeField]
    public List<string> shortChoiseText;
    [TextArea]
    [SerializeField]
    public string description;

    [FoldoutGroup("Event Efects")]
    [HorizontalGroup("Event Efects/Bools")]
    [VerticalGroup("Event Efects/Bools/Left")]
    [SerializeField]
    public bool hasUpAction = false;
    [FoldoutGroup("Event Efects")]
    [VerticalGroup("Event Efects/Bools/Right")]
    [SerializeField]
    public bool hasDownAction = false;
    [FoldoutGroup("Event Efects")]
    [SerializeField]
    public Dictionary<ParameterType, int> LeftEffect = new Dictionary<ParameterType, int>();
    [FoldoutGroup("Event Efects")]
    [SerializeField]
    public Dictionary<ParameterType, int> RightEffect = new Dictionary<ParameterType, int>();
    [FoldoutGroup("Event Efects")]
    [ShowIf("hasUpAction")]
    [SerializeField]
    public Dictionary<ParameterType, int> UpEffect = new Dictionary<ParameterType, int>();
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using TMPro;

public class GameEvent : SerializedMonoBehaviour
{
    GameEventTemplate geTemplate;
    public bool toDelete = false;

    [HorizontalGroup("Basic Info")]
    [LabelWidth(50)]
    [SerializeField]
    public GameEventType type;

    [FoldoutGroup("Text Vars")]
    [SerializeField]
    public List<string> shortChoiseText;

    [FoldoutGroup("Movement Control")]
    [SerializeField]
    float movementSpeed = 1f;
    [FoldoutGroup("Movement Control")]
    [SerializeField]
    float movementDown = 0.1f;
    [FoldoutGroup("Movement Control")]
    [SerializeField]
    float rotationSpeed = 1f;
    [FoldoutGroup("Movement Control")]
    [SerializeField]
    float rotationAngle = 15f;
    [FoldoutGroup("Movement Control")]
    [SerializeField]
    public int sortingOrder = 0;
    [FoldoutGroup("Movement Control")]
    bool isMoving = false;

    [FoldoutGroup("Event Efects")]
    [HorizontalGroup("Event Efects/Bools")]
    [VerticalGroup("Event Efects/Bools/Left")]
    [SerializeField]
    public bool hasUpAction = false;
    [FoldoutGroup("Event Efects")]
    [VerticalGroup("Event Efects/Bools/Right")]
    [SerializeField]
    public bool hasDownAction = false;
    [FoldoutGroup("Event Efects")]
    [SerializeField]
    public Dictionary<ParameterType, int> LeftEffect = new Dictionary<ParameterType, int>();
    [FoldoutGroup("Event Efects")]
    [SerializeField]
    public Dictionary<ParameterType, int> RightEffect = new Dictionary<ParameterType, int>();
    [FoldoutGroup("Event Efects")]
    [ShowIf("hasUpAction")]
    [SerializeField]
    public Dictionary<ParameterType, int> UpEffect = new Dictionary<ParameterType, int>();

    [FoldoutGroup("UI Elements")]
    [SerializeField]
    Image artUI;
    [FoldoutGroup("UI Elements")]
    [SerializeField]
    TextMeshProUGUI descriptionUI;
    [FoldoutGroup("UI Elements")]
    [SerializeField]
    Canvas canvasUI;
    [FoldoutGroup("UI Elements")]
    [SerializeField]
    List<TextMeshProUGUI> shortChoiseTextUI;

    Vector2 startPosition;
    [SerializeField]
    Vector2 moveToPosition;
    Quaternion rotateTo;
    int showTextIdx;
    bool showTextFlg;

    void Start()
    {
        startPosition = transform.position;
        moveToPosition = transform.position;
        rotateTo = Quaternion.identity;
        showTextIdx = -1;
        showTextFlg = false;
        for(int i=0; i<shortChoiseText.Count; i++)
        {
            shortChoiseTextUI[i].text = shortChoiseText[i];
        }
    }

    public void Randomize(int order)
    {
        SetOrder(order);
        int randomType = Random.Range(1, System.Enum.GetValues(typeof(GameEventType)).Length - 1);
        type = (GameEventType)randomType;
        int randomGETId = Random.Range(0, 2);
        descriptionUI.text = randomGETId.ToString();
        LoadGET(randomGETId.ToString());
    }

    public void DeathEvent(int parameter, int value)
    {
        SetOrder(-1);
        type = GameEventType.Death;
        LoadGET(parameter.ToString() + (value <= 0 ? 0 : 1).ToString());
    }

    // Update is called once per frame
    void Update()
    {
        if (!isMoving)
        {
            if (Vector2.Distance(transform.position, moveToPosition) < 0.05f)
            {
                transform.position = moveToPosition;
            }
            transform.position = Vector3.Lerp(transform.position, moveToPosition, movementSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotateTo, rotationSpeed * Time.deltaTime);
            if (showTextIdx != -1)
            {
                shortChoiseTextUI[showTextIdx].transform.rotation = Quaternion.Lerp(shortChoiseTextUI[showTextIdx].transform.rotation, Quaternion.identity, rotationSpeed * Time.deltaTime);
                shortChoiseTextUI[showTextIdx].alpha = Mathf.Lerp(shortChoiseTextUI[showTextIdx].alpha, (showTextFlg ? 1f : 0f), rotationSpeed * Time.deltaTime);
            }
        }
    }

    public void ChoiseAnimate(Vector2 dest, bool move)
    {
        if (!isMoving)
        {
            if (move)
            {
                if (!hasDownAction && dest.y < 0)
                {
                    dest.y = 0;
                    return;
                }
                if (!hasUpAction && dest.y > 0)
                {
                    dest.y = 0;
                    return;
                }
                dest.y = movementDown * dest.y;
                moveToPosition = startPosition + 2f * dest;
                rotateTo = Quaternion.Euler(0f, 0f, -rotationAngle * dest.x);
                if (showTextIdx != -1) shortChoiseTextUI[showTextIdx].alpha = 0; // hide previous text
                showTextIdx = (dest.x == -1 ? 0 : (dest.x == 1 ? 1 : (hasUpAction && dest.y > 0 ? 2 : (hasDownAction && dest.y < 0 ? 3 : -1))));
                showTextFlg = true;
            }
            else
            {
                moveToPosition = startPosition;
                rotateTo = Quaternion.identity;
                showTextFlg = false;
            }
        }
    }

    public Dictionary<ParameterType, int> Confirm(float choise)
    {
        if (!isMoving)
        {
            switch (choise)
            {
                case (-1):
                    return LeftEffect;
                case (1):
                    return RightEffect;
                default:
                    return null;
            }
        }
        return null;
    }

    public void SetOrder(int order)
    {
        sortingOrder = order;
        GetComponent<SpriteRenderer>().sortingOrder = order;
        canvasUI.sortingOrder = order;
    }

    public IEnumerator MoveLayer(int dir)
    {
        int phase = 0;
        isMoving = true;
        List<Vector3> movePhase = new List<Vector3>();
        movePhase.Add(transform.position + 3f * new Vector3(dir, 0, 0));
        movePhase.Add(transform.position);
        List<Quaternion> rotatePhase = new List<Quaternion>();
        rotatePhase.Add(Quaternion.Euler(0f, 0f, -rotationAngle * dir));
        rotatePhase.Add(Quaternion.identity);
        while (phase < 2)
        {

            if (Vector3.Distance(transform.position, movePhase[phase]) < 0.005f)
            {
                transform.position = movePhase[phase];
                SetOrder(dir);
                phase++;
                continue;
            }
            transform.position = Vector3.Lerp(transform.position, movePhase[phase], 3f * movementSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotatePhase[phase], 3f * rotationSpeed * Time.deltaTime);
            yield return null;
        }
        isMoving = false;
    }

    public IEnumerator MoveConfirm(int dir, GameEvent nextEvent)
    {
        int phase = 0;
        isMoving = true;
        List<Vector3> movePhase = new List<Vector3>
        {
            transform.position + 1f * new Vector3(dir, 0, 0),
            transform.position + 12f * new Vector3(dir, -0.6f, 0)
        };
        List<Quaternion> rotatePhase = new List<Quaternion>
        {
            Quaternion.Euler(0f, 0f, -rotationAngle * dir),
            Quaternion.Euler(0f, 0f, -90 * dir)
        };
        while (phase < 2)
        {

            if (Vector3.Distance(transform.position, movePhase[phase]) < 0.05f)
            {
                transform.position = movePhase[phase];
                SetOrder(dir);
                phase++;
                continue;
            }
            transform.position = Vector3.Lerp(transform.position, movePhase[phase], (phase == 0 ? 1f : 3f) * movementSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotatePhase[phase], 1f * rotationSpeed * Time.deltaTime);
            yield return null;
        }
        Destroy(this.gameObject);
        if(nextEvent != null)
        {
            nextEvent.SetOrder(1);
        }
    }

    void LoadGET(string id)
    {
        GameEventTemplate geTemplate = Resources.Load<GameEventTemplate>("Prefabs/GETemplates/" + type.ToString() + "/" + type.ToString() + " " + id);
        Debug.Log("Prefabs/GETemplates/" + type.ToString() + "/" + type.ToString() + " " + id);
        hasDownAction = geTemplate.hasDownAction;
        hasUpAction = geTemplate.hasUpAction;
        LeftEffect = geTemplate.LeftEffect;
        RightEffect = geTemplate.RightEffect;
        UpEffect = geTemplate.UpEffect;

        artUI.sprite = geTemplate.Art;
        descriptionUI.text = geTemplate.description;
        shortChoiseText = geTemplate.shortChoiseText;
    }
}

// GENERATED AUTOMATICALLY FROM 'Assets/Resources/InputControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControls"",
    ""maps"": [
        {
            ""name"": ""Action"",
            ""id"": ""6c098fcd-1ece-470e-87a7-8902e3e485df"",
            ""actions"": [
                {
                    ""name"": ""Choise"",
                    ""type"": ""Value"",
                    ""id"": ""73353d00-d2aa-4136-a490-c550ed224d23"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold(pressPoint=0.5)""
                },
                {
                    ""name"": ""ConfirmLeftWASD"",
                    ""type"": ""Button"",
                    ""id"": ""07472f26-a037-4ccc-967b-e1fcf72b0a8d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                },
                {
                    ""name"": ""ConfirmRightWASD"",
                    ""type"": ""Button"",
                    ""id"": ""7b2d82b7-7e34-4191-a697-1ffb4118f65a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                },
                {
                    ""name"": ""ConfirmUpWASD"",
                    ""type"": ""Button"",
                    ""id"": ""4153f831-bd99-40c5-b7fa-07e942ab56f6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                },
                {
                    ""name"": ""ConfirmDownWASD"",
                    ""type"": ""Button"",
                    ""id"": ""8b9f87c5-6b2a-485f-9b53-d9b79b008254"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                },
                {
                    ""name"": ""ConfirmLeftArrow"",
                    ""type"": ""Button"",
                    ""id"": ""d1d4bcd0-95d9-40e5-abab-68d877c047d6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                },
                {
                    ""name"": ""ConfirmRightArrow"",
                    ""type"": ""Button"",
                    ""id"": ""b1ea3ba5-9d7b-44c4-b085-0da78fde19d3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                },
                {
                    ""name"": ""ConfirmUpArrow"",
                    ""type"": ""Button"",
                    ""id"": ""098eb8d3-d70e-477f-9b47-24800bc65f45"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                },
                {
                    ""name"": ""ConfirmDownArrow"",
                    ""type"": ""Button"",
                    ""id"": ""1cdc2c69-ccc4-45dc-b2a8-d8ee20512c67"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap(tapTime=1,tapDelay=1)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""71a7ae2c-93fa-4514-a82d-c9b350fba5ef"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""763f7838-c368-43f7-8da3-79debafea3e0"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""44883d74-dda5-4f2c-b65c-c9cd7c420671"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""229aaf89-3550-46cf-97b2-2301e192caff"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""dedcc0ce-0ade-4174-9ed7-b876a38e31ed"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""17d12057-38f3-4609-a1a3-4ee49fba4b00"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""c0e27979-37d9-4c08-a485-fd15db79581b"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""3dda3a3e-be74-4ee2-a989-3f42b30b3f9a"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""13baca21-9776-421b-9ea7-40d873e1c9dd"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""43d9eeb3-cb6f-4d20-a1a6-69bb3f1b3fb3"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Choise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""76a0c9e4-55a6-40c4-b4e4-a7d372e42217"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmLeftWASD"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bd136ac5-9013-4103-a7d3-d377f718f34a"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmLeftArrow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""edf68f89-7d8c-48f1-8f4c-1d9e26b6b44a"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmRightArrow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""54206ee0-8ca0-4379-bb30-419c50d46efa"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmUpArrow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""483d7aa2-b751-4fff-8395-fa56130e6efa"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmDownArrow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f6583d20-f2d1-480d-a2b6-3b3289fd7542"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmRightWASD"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1dad0f8b-a7cb-4069-94f8-cd12d4eec637"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmUpWASD"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d46a7cf3-d7ef-444d-bf50-5f25f24bdb66"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmDownWASD"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Action
        m_Action = asset.FindActionMap("Action", throwIfNotFound: true);
        m_Action_Choise = m_Action.FindAction("Choise", throwIfNotFound: true);
        m_Action_ConfirmLeftWASD = m_Action.FindAction("ConfirmLeftWASD", throwIfNotFound: true);
        m_Action_ConfirmRightWASD = m_Action.FindAction("ConfirmRightWASD", throwIfNotFound: true);
        m_Action_ConfirmUpWASD = m_Action.FindAction("ConfirmUpWASD", throwIfNotFound: true);
        m_Action_ConfirmDownWASD = m_Action.FindAction("ConfirmDownWASD", throwIfNotFound: true);
        m_Action_ConfirmLeftArrow = m_Action.FindAction("ConfirmLeftArrow", throwIfNotFound: true);
        m_Action_ConfirmRightArrow = m_Action.FindAction("ConfirmRightArrow", throwIfNotFound: true);
        m_Action_ConfirmUpArrow = m_Action.FindAction("ConfirmUpArrow", throwIfNotFound: true);
        m_Action_ConfirmDownArrow = m_Action.FindAction("ConfirmDownArrow", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Action
    private readonly InputActionMap m_Action;
    private IActionActions m_ActionActionsCallbackInterface;
    private readonly InputAction m_Action_Choise;
    private readonly InputAction m_Action_ConfirmLeftWASD;
    private readonly InputAction m_Action_ConfirmRightWASD;
    private readonly InputAction m_Action_ConfirmUpWASD;
    private readonly InputAction m_Action_ConfirmDownWASD;
    private readonly InputAction m_Action_ConfirmLeftArrow;
    private readonly InputAction m_Action_ConfirmRightArrow;
    private readonly InputAction m_Action_ConfirmUpArrow;
    private readonly InputAction m_Action_ConfirmDownArrow;
    public struct ActionActions
    {
        private @InputControls m_Wrapper;
        public ActionActions(@InputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Choise => m_Wrapper.m_Action_Choise;
        public InputAction @ConfirmLeftWASD => m_Wrapper.m_Action_ConfirmLeftWASD;
        public InputAction @ConfirmRightWASD => m_Wrapper.m_Action_ConfirmRightWASD;
        public InputAction @ConfirmUpWASD => m_Wrapper.m_Action_ConfirmUpWASD;
        public InputAction @ConfirmDownWASD => m_Wrapper.m_Action_ConfirmDownWASD;
        public InputAction @ConfirmLeftArrow => m_Wrapper.m_Action_ConfirmLeftArrow;
        public InputAction @ConfirmRightArrow => m_Wrapper.m_Action_ConfirmRightArrow;
        public InputAction @ConfirmUpArrow => m_Wrapper.m_Action_ConfirmUpArrow;
        public InputAction @ConfirmDownArrow => m_Wrapper.m_Action_ConfirmDownArrow;
        public InputActionMap Get() { return m_Wrapper.m_Action; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ActionActions set) { return set.Get(); }
        public void SetCallbacks(IActionActions instance)
        {
            if (m_Wrapper.m_ActionActionsCallbackInterface != null)
            {
                @Choise.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnChoise;
                @Choise.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnChoise;
                @Choise.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnChoise;
                @ConfirmLeftWASD.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmLeftWASD;
                @ConfirmLeftWASD.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmLeftWASD;
                @ConfirmLeftWASD.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmLeftWASD;
                @ConfirmRightWASD.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmRightWASD;
                @ConfirmRightWASD.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmRightWASD;
                @ConfirmRightWASD.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmRightWASD;
                @ConfirmUpWASD.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmUpWASD;
                @ConfirmUpWASD.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmUpWASD;
                @ConfirmUpWASD.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmUpWASD;
                @ConfirmDownWASD.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmDownWASD;
                @ConfirmDownWASD.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmDownWASD;
                @ConfirmDownWASD.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmDownWASD;
                @ConfirmLeftArrow.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmLeftArrow;
                @ConfirmLeftArrow.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmLeftArrow;
                @ConfirmLeftArrow.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmLeftArrow;
                @ConfirmRightArrow.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmRightArrow;
                @ConfirmRightArrow.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmRightArrow;
                @ConfirmRightArrow.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmRightArrow;
                @ConfirmUpArrow.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmUpArrow;
                @ConfirmUpArrow.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmUpArrow;
                @ConfirmUpArrow.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmUpArrow;
                @ConfirmDownArrow.started -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmDownArrow;
                @ConfirmDownArrow.performed -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmDownArrow;
                @ConfirmDownArrow.canceled -= m_Wrapper.m_ActionActionsCallbackInterface.OnConfirmDownArrow;
            }
            m_Wrapper.m_ActionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Choise.started += instance.OnChoise;
                @Choise.performed += instance.OnChoise;
                @Choise.canceled += instance.OnChoise;
                @ConfirmLeftWASD.started += instance.OnConfirmLeftWASD;
                @ConfirmLeftWASD.performed += instance.OnConfirmLeftWASD;
                @ConfirmLeftWASD.canceled += instance.OnConfirmLeftWASD;
                @ConfirmRightWASD.started += instance.OnConfirmRightWASD;
                @ConfirmRightWASD.performed += instance.OnConfirmRightWASD;
                @ConfirmRightWASD.canceled += instance.OnConfirmRightWASD;
                @ConfirmUpWASD.started += instance.OnConfirmUpWASD;
                @ConfirmUpWASD.performed += instance.OnConfirmUpWASD;
                @ConfirmUpWASD.canceled += instance.OnConfirmUpWASD;
                @ConfirmDownWASD.started += instance.OnConfirmDownWASD;
                @ConfirmDownWASD.performed += instance.OnConfirmDownWASD;
                @ConfirmDownWASD.canceled += instance.OnConfirmDownWASD;
                @ConfirmLeftArrow.started += instance.OnConfirmLeftArrow;
                @ConfirmLeftArrow.performed += instance.OnConfirmLeftArrow;
                @ConfirmLeftArrow.canceled += instance.OnConfirmLeftArrow;
                @ConfirmRightArrow.started += instance.OnConfirmRightArrow;
                @ConfirmRightArrow.performed += instance.OnConfirmRightArrow;
                @ConfirmRightArrow.canceled += instance.OnConfirmRightArrow;
                @ConfirmUpArrow.started += instance.OnConfirmUpArrow;
                @ConfirmUpArrow.performed += instance.OnConfirmUpArrow;
                @ConfirmUpArrow.canceled += instance.OnConfirmUpArrow;
                @ConfirmDownArrow.started += instance.OnConfirmDownArrow;
                @ConfirmDownArrow.performed += instance.OnConfirmDownArrow;
                @ConfirmDownArrow.canceled += instance.OnConfirmDownArrow;
            }
        }
    }
    public ActionActions @Action => new ActionActions(this);
    public interface IActionActions
    {
        void OnChoise(InputAction.CallbackContext context);
        void OnConfirmLeftWASD(InputAction.CallbackContext context);
        void OnConfirmRightWASD(InputAction.CallbackContext context);
        void OnConfirmUpWASD(InputAction.CallbackContext context);
        void OnConfirmDownWASD(InputAction.CallbackContext context);
        void OnConfirmLeftArrow(InputAction.CallbackContext context);
        void OnConfirmRightArrow(InputAction.CallbackContext context);
        void OnConfirmUpArrow(InputAction.CallbackContext context);
        void OnConfirmDownArrow(InputAction.CallbackContext context);
    }
}

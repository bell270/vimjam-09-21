using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private static LevelManager instance;
    [SerializeField] private Animator fadeAnimator;
    public static LevelManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
    }

    public void LoadGame()
    {
        StartCoroutine(LevelLoading(2, 1));
    }

    public void LoadMenu()
    {
        StartCoroutine(LevelLoading(1, 2));
    }

    public void ReloadGame()
    {
        StartCoroutine(LevelReloading(2));
    }

    IEnumerator LevelLoading(int loadIdx, int unloadIdx)
    {
        fadeAnimator.SetBool("Faded", true);
        yield return new WaitForSeconds(1.5f);
        SceneManager.UnloadSceneAsync(unloadIdx);
        SceneManager.LoadScene(loadIdx, LoadSceneMode.Additive);
        fadeAnimator.SetBool("Faded", false);
    }

    IEnumerator LevelReloading(int loadIdx)
    {
        fadeAnimator.SetBool("Faded", true);
        SceneManager.UnloadSceneAsync(loadIdx);
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(loadIdx, LoadSceneMode.Additive);
        fadeAnimator.SetBool("Faded", false);
    }
}

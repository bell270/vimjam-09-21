
public enum GameEventType
{
    Death
    , Bank /* Authority, Finances */
    , Ally /* Authority, Interest, Finances */
    , Wife /* Authority, Family */
    , Cop /* Finances, Interest */
    , Kid /* Finances, Family */
    , Reporter /* Interest, Family */
    , Enemy /* Authority, Interest, Finances */
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClick : MonoBehaviour
{
    bool clicked = false;
    public void PlayGame()
    {
        if (!clicked)
        {
            clicked = true;
            LevelManager.GetInstance().LoadGame();
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

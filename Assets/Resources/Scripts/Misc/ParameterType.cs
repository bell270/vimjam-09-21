public enum ParameterType 
{
    Authority
    , Finances
    , Interest
    , Family
}
